package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Oblandefragment8 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna36, R.drawable.oblatna37,R.drawable.oblatna38,R.drawable.oblatna39,R.drawable.oblatna40};
    private View view;
    public Oblandefragment8() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna8),0 ,2500);
        return view;
    }
}
