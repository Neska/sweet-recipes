package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kolacfragment3 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.coko1, R.drawable.coko2, R.drawable.coko3, R.drawable.coko4, R.drawable.coko5};
    private View view;
    public Kolacfragment3() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);
        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac3),0 , 2500);

        return view;
    }
}
