package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Kompotfragment8 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.suhovoce, R.drawable.suhovoce};
    private View view;
    public Kompotfragment8() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kompot8),0 ,2500);
        return view;
    }
}

