package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kompotfragment7 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.bijeletresnje, R.drawable.bijeletresnje};
    private View view;
    public Kompotfragment7() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kompot7),0 ,2500);
        return view;
    }
}
