package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tortefragment4 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.torta10, R.drawable.torta11,R.drawable.torta12};
    private View view;
    public Tortefragment4() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tortefragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.torta4),0 ,2500);
        return view;
    }
}
