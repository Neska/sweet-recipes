package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Rolatfragment6 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.split1, R.drawable.split2,R.drawable.split3,R.drawable.split4,R.drawable.split5};
    private View view;
    public Rolatfragment6() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat6),0 ,2500);
        return view;
    }
}