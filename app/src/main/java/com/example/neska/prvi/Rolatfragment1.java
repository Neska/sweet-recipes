package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Rolatfragment1 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.banana1, R.drawable.banana2,R.drawable.banana3,R.drawable.banana4,R.drawable.banana5};
    private View view;
    public Rolatfragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat1),0 ,2500);
        return view;
    }
}
