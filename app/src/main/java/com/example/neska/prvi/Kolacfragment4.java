package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kolacfragment4 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.cokoo1,R.drawable.cokoo2,R.drawable.cokoo3,R.drawable.cokoo4};
    private View view;
    public Kolacfragment4() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);
        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac4), R.string.cudoodcokolade ,2500);

        return view;
    }
}
