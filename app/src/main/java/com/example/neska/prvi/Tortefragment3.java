package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Tortefragment3 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.torta7, R.drawable.torta8,R.drawable.torta9};
    private View view;
    public Tortefragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tortefragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.torta03),0 ,2500);
        return view;
    }
}