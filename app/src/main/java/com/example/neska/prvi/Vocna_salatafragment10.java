package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Vocna_salatafragment10 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.voce19,R.drawable.voce19,R.drawable.voce19};
    private View view;
    public Vocna_salatafragment10() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vocna_salatafragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.salata10),0 ,2500);
        return view;
    }
}