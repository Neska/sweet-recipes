package com.example.neska.prvi;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;


public class Klasa1 extends Fragment {

    private ViewPager viewPager;
    private Handler handler;
    private int currentPage, numPages, delay;

    public Klasa1(){}
    

    public void PageContent(View view, Context context, Integer[] images, String webViewData, int videoData, int slidingImgTime)
    {
        currentPage = 0;
        delay = slidingImgTime;

        ArrayList<Integer> imagesArray = new ArrayList<Integer>(images.length);

        for (int i = 0; i < images.length; i++)
            imagesArray.add(images[i]);
        
        TextView textView = (TextView) view.findViewById(R.id.videoLink);
        
        if (videoData!=0) {
            textView.setText(videoData);
        }
        else
            textView.setVisibility(View.GONE);


        ScrollView scrollView = (ScrollView) view.findViewById(R.id.container);
        scrollView.smoothScrollTo(0, 0);


        WebView webView = (WebView) view.findViewById(R.id.webView1);

        webView.loadData(String.format(webViewData), "text/html", "utf-8");

        webView.getSettings();
        webView.setBackgroundColor(Color.parseColor("#EBF5FB"));

        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(new SlidingImage_Adapter(context, imagesArray));

        CirclePageIndicator pageIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        pageIndicator.setViewPager(viewPager);

        final float density = getResources().getDisplayMetrics().density;
        pageIndicator.setRadius(5 * density);

        numPages = images.length;

        pageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });

        handler = new Handler();
        handler.postDelayed(timer, 50);

    }

    private Runnable timer = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            if (currentPage == numPages)
                currentPage = 0;
            viewPager.setCurrentItem(currentPage++, true);
      /* and here comes the "trick" */
            handler.postDelayed(this, delay);
        }
    };
}
