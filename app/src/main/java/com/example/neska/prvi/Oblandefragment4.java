package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Oblandefragment4 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna16, R.drawable.oblatna17,R.drawable.oblatna18,R.drawable.oblatna19,R.drawable.oblatna20};
    private View view;
    public Oblandefragment4() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna4),0 ,2500);
        return view;
    }
}