package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class KokosKolacfragment1 extends Klasa1 {

    private static final Integer[] IMAGES = {R.drawable.rafaelo1, R.drawable.rafaelo2,R.drawable.rafaelo3,R.drawable.rafaelo4};
    private View view;
    public KokosKolacfragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      view =  inflater.inflate(R.layout.fragment_kokos_kolacfragment1, container, false);

      PageContent(view, getActivity(), IMAGES, getString(R.string.kokos1),0 ,2500);


        return view;
    }

}
