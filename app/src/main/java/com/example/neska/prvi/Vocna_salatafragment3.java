package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Vocna_salatafragment3 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.voce6,R.drawable.voce6,R.drawable.voce6};
    private View view;
    public Vocna_salatafragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vocna_salatafragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.salata3),0 ,2500);
        return view;
    }
}
