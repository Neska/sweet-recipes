package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class KokosKolacfragment9 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.kokos_kocke1, R.drawable.kokos_kocke1};
    private View view;
    public KokosKolacfragment9() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_kokos_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kokos9),0 ,2500);


        return view;
    }

}