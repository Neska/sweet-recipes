package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Rolatfragment2 extends Klasa1 {

    private static final Integer[] IMAGES = {R.drawable.banana6, R.drawable.banana7,R.drawable.banana8,R.drawable.banana9,R.drawable.banana10};
    private View view;
    public Rolatfragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat2),0 ,2500);
        return view;
    }
}
