package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Rolatfragment3 extends Klasa1 {

    private static final Integer[] IMAGES = {R.drawable.banana11, R.drawable.banana12,R.drawable.banana13,R.drawable.banana14,R.drawable.banana15};
    private View view;
    public Rolatfragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat3),0 ,2500);
        return view;
    }
}