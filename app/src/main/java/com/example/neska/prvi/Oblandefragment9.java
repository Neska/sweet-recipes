package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Oblandefragment9 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna41, R.drawable.oblatna42,R.drawable.oblatna43,R.drawable.oblatna44,R.drawable.oblatna45};
    private View view;
    public Oblandefragment9() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna9),0 ,2500);
        return view;
    }
}