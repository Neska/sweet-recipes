package com.example.neska.prvi;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class Kolacfragment7 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.americki, R.drawable.americki2,R.drawable.americki3,R.drawable.americki4,R.drawable.americki5,R.drawable.americki6};    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private View view;
    public Kolacfragment7() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);
        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac7),R.string.americki,2500);

        return view;
    }
}
