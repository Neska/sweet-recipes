package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Slatke_pitefragment7 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.pita19, R.drawable.pita20,R.drawable.pita21};
    private View view;
    public Slatke_pitefragment7() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slatke_pitefragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.pita7),0 ,2500);
        return view;
    }
}
