package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuhiKolacfragment2 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.suhi4, R.drawable.suhi5,R.drawable.suhi6,R.drawable.suhi7};
    private View view;
    public SuhiKolacfragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slatke_pitefragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.suhi2),0 ,2500);
        return view;
    }
}
