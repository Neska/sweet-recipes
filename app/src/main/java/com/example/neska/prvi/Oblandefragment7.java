package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Oblandefragment7 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna31, R.drawable.oblatna32,R.drawable.oblatna33,R.drawable.oblatna34,R.drawable.oblatna35};
    private View view;
    public Oblandefragment7() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna7),0 ,2500);
        return view;
    }
}