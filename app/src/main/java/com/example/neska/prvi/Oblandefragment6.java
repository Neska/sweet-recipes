package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Oblandefragment6 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna26, R.drawable.oblatna27,R.drawable.oblatna28,R.drawable.oblatna29,R.drawable.oblatna30};
    private View view;
    public Oblandefragment6() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna6),0 ,2500);
        return view;
    }
}