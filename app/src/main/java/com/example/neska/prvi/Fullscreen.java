package com.example.neska.prvi;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

public class Fullscreen extends AppCompatActivity {


    View mediaplayerL;
    ScrollView sv;
    ImageButton playbtn1,btn,fullScreen;
    SeekBar seeker;
    int position = 0;
    int progress = 1;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    VideoView mVideoView2;
    MediaController mediaController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);


       mVideoView2 = (VideoView)findViewById(R.id.myvideoview);
        String uriPath2 = "android.resource://com.example.neska.prvi/"+R.raw.movie;
        Uri uri2 = Uri.parse(uriPath2);
        mVideoView2.setVideoURI(uri2);
        mVideoView2.requestFocus();

     mediaPlayer();

    }


    public void mediaPlayer ()
    {

        playbtn1 = (ImageButton) findViewById(R.id.playVideo2);
        fullScreen = (ImageButton) findViewById(R.id.fullScreen);
        seeker = (SeekBar) findViewById(R.id.seeker);
        mediaplayerL = (View) findViewById(R.id.mediaplayer_menu);


        fullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Fullscreen.this, Kolacfragment1.class);
                startActivity(intent);

            }
        });

        playbtn1.setBackgroundResource(R.drawable.ic_play);


        playbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!mVideoView2.isPlaying()){
                    playbtn1.setBackgroundResource(R.drawable.ic_play);
                    position = mVideoView2.getCurrentPosition();
                    mVideoView2.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {


                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            Log.d("ads", String.valueOf(mVideoView2.getDuration()));
                            mVideoView2.seekTo(position);
                        }
                    });
                    mVideoView2.start();

                }
                else{
                    playbtn1.setBackgroundResource(R.drawable.ic_pause);
                    position = mVideoView2.getCurrentPosition();
                    mVideoView2.pause();
                }

            }
        });

        seeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (b)
                    mVideoView2.seekTo((mVideoView2.getDuration())*seeker.getProgress()/100 );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });


        mVideoView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (mediaplayerL.getVisibility() == View.VISIBLE)
                    mediaplayerL.setVisibility(View.INVISIBLE);

                else {
                    Log.d("visibel","is");
                    mediaplayerL.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });


        if (!mVideoView2.isPlaying()) {
            final Runnable Update = new Runnable() {
                public void run() {

                    position = mVideoView2.getCurrentPosition();
                    progress = position * 100 / mVideoView2.getDuration();

                    if (seeker.getProgress() <= 100) {
                        seeker.setProgress(progress);
                    }
                }
            };

            Timer(50,50, Update);


        }

        final Runnable hideShowPlayerMenu = new Runnable()
        {
            public void run()
            {
                if (mVideoView2.isPlaying())
                    mediaplayerL.setVisibility(View.INVISIBLE);
            }
        };

        Timer(7000, 7000, hideShowPlayerMenu);
    }

    public void Timer (int delay, int period, final Runnable update)
    {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, delay, period);
    }


}
