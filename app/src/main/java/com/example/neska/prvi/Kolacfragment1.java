package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kolacfragment1 extends Klasa1 {

    private static final Integer[] IMAGES = {R.drawable.kolac1, R.drawable.kolac2,R.drawable.kolac3,R.drawable.kolac4};
    private View view;
    public Kolacfragment1() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac1),R.string.kinder_pinguinic ,2500);
        return view;
    }
}

