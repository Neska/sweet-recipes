package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Oblandefragment2 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna6, R.drawable.oblatna7,R.drawable.oblatna8,R.drawable.oblatna9,R.drawable.oblatna10};
    private View view;
    public Oblandefragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna2),0 ,2500);
        return view;
    }
}
