package com.example.neska.prvi;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kolacfragment6 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.fantazija1, R.drawable.fantazija2,R.drawable.fantazija4,R.drawable.fantazija5};
    private View view;
    public Kolacfragment6() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);
        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac6),R.string.fantazija , 2500);

        return view;
    }
}
