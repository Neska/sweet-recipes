package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Kompotfragment3 extends Klasa1 {

    private static final Integer[] IMAGES = {R.drawable.breskve, R.drawable.breskve};
    private View view;
    public Kompotfragment3() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kompot3),0 ,2500);
        return view;
    }
}
