package com.example.neska.prvi;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class KokosKolacfragment3 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.kokos0, R.drawable.kokos1, R.drawable.kokos3, R.drawable.kokos4, R.drawable.kokos5};
    private View view;
    public KokosKolacfragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_kokos_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kokos3),0 ,2500);


        return view;
    }

}