package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Kolacfragment5 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.flodni, R.drawable.flodni2,R.drawable.flodni3,R.drawable.flodni4};
    private View view;
    public Kolacfragment5() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);
        PageContent(view, getActivity(), IMAGES, getString(R.string.kolac5),R.string.flodni , 2500);

        return view;
    }
}

