package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Slatka_pecivafragment2 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.pecivo4, R.drawable.pecivo5,R.drawable.pecivo6};
    private View view;
    public Slatka_pecivafragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slatka_pecivafragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.pecivo2),0 ,2500);
        return view;
    }
}
