package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuhiKolacfragment3 extends Klasa1 {



    private static final Integer[] IMAGES = {R.drawable.suhi8, R.drawable.suhi9,R.drawable.suhi10};
    private View view;
    public SuhiKolacfragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_suhi_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.suhi3),0 ,2500);
        return view;
    }
}
