package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Kompotfragment1 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.kompot1, R.drawable.kompot2,R.drawable.kompot3};
    private View view;
    public Kompotfragment1() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.kompot1),0 ,2500);
        return view;
    }
}

