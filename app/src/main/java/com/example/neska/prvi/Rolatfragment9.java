package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Rolatfragment9 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.split16, R.drawable.split17,R.drawable.split18,R.drawable.split19,R.drawable.split20};
    private View view;
    public Rolatfragment9() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat9),0 ,2500);
        return view;
    }
}
