package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Rolatfragment7 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.split6, R.drawable.split7,R.drawable.split8,R.drawable.split9,R.drawable.split10};
    private View view;
    public Rolatfragment7() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat7),0 ,2500);
        return view;
    }
}