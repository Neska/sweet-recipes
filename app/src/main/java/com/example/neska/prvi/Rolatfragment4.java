package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Rolatfragment4 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.banana16, R.drawable.banana17,R.drawable.banana18,R.drawable.banana19,R.drawable.banana20};
    private View view;
    public Rolatfragment4() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat4),0 ,2500);
        return view;
    }
}