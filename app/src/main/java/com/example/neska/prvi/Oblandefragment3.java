package com.example.neska.prvi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Oblandefragment3 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.oblatna11, R.drawable.oblatna12,R.drawable.oblatna13,R.drawable.oblatna14,R.drawable.oblatna15};
    private View view;
    public Oblandefragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kolacfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.oblatna3),0 ,2500);
        return view;
    }
}