package com.example.neska.prvi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Slatka_pecivafragment1 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.pecivo1, R.drawable.pecivo2,R.drawable.pecivo3};
    private View view;
    public Slatka_pecivafragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slatka_pecivafragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.pecivo1),0 ,2500);
        return view;
    }
}
