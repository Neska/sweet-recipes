package com.example.neska.prvi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Rolatfragment8 extends Klasa1 {


    private static final Integer[] IMAGES = {R.drawable.split11, R.drawable.split12,R.drawable.split13,R.drawable.split14,R.drawable.split15};
    private View view;
    public Rolatfragment8() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rolatfragment1, container, false);

        PageContent(view, getActivity(), IMAGES, getString(R.string.rolat8),0 ,2500);
        return view;
    }
}