package com.example.neska.prvi;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        fragmentManager = getFragmentManager();
        switch (id) {
            case R.id.kolaci:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Kolaci()).commit();
                break;


            case R.id.kolaci_s_kokosom:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Kolaci_s_kokosom()).commit();
                break;

            case R.id.kompoti:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Kompoti()).commit();
                break;

            case R.id.oblande:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Oblande()).commit();
                break;

            case R.id.rolati:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Rolati()).commit();
                break;

            case R.id.slatka_peciva:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Vocne_salate()).commit();
                break;

            case R.id.slatke_pite:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Slatka_peciva()).commit();
                break;

            case R.id.suhi_kolaci:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Slatke_pite()).commit();
                break;

            case R.id.torte_s_kokosom:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Suhi_kolaci()).commit();
                break;

            case R.id.torte:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Torte()).commit();
                break;

            case R.id.vocne_salate:
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Torte_s_kokosom()).commit();
                break;


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    }
