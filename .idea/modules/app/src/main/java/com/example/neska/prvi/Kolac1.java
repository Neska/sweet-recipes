package com.example.neska.prvi;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.webkit.WebView;
import android.widget.TextView;

public class Kolac1 extends AppCompatActivity {

    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kolac1);


        String htmlText = " %s ";
        String myData = "Hello World! This tutorial is to show demo of displaying text with justify alignment in WebView.";

        WebView webView = (WebView) findViewById(R.id.webView1);
        String text = getString(R.string.onama1);

        webView.loadData(String.format(text), "text/html", "utf-8");

        webView.getSettings();
        webView.setBackgroundColor(Color.parseColor("#EBF5FB"));




    }


    }

